package com.namadi.android.mnamadicom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class MainActivity extends Activity  implements OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((Button)findViewById(R.id.about)).setOnClickListener(this);
        ((Button)findViewById(R.id.play)).setOnClickListener(this);
        ((Button)findViewById(R.id.hiscore)).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.about) {
            Intent intent = new Intent(MainActivity.this, About.class);
            startActivity(intent);
        }

        if(v.getId() == R.id.play) {
            Intent intent = new Intent(MainActivity.this, Play.class);
            startActivity(intent);
        }

        if(v.getId() == R.id.hiscore) {
            startActivity(new Intent(MainActivity.this, HiScore.class));
        }
    }
}
