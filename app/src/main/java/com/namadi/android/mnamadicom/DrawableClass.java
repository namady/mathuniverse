package com.namadi.android.mnamadicom;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;

/**
 * Created by mNamadi on 21/09/15.
 */
public class DrawableClass extends Drawable {

    Paint mPaintBackground;
    RectF mRectF;
    Paint mPaintProgress;

    private int mSweepAngle;
    private int mStartAngle;
    private int mStep;

    DrawableClass() {
        this.mStartAngle = 270;
        this.mSweepAngle = 0;

        mPaintProgress = new Paint();
        mPaintProgress.setAntiAlias(true);
        mPaintProgress.setStyle(Paint.Style.STROKE);
        mPaintProgress.setStrokeWidth(200);
        mPaintProgress.setStrokeCap(Paint.Cap.ROUND);
        mPaintProgress.setColor(Color.GREEN);

        mPaintBackground = new Paint();
        mPaintBackground.setAntiAlias(true);
        mPaintBackground.setStyle(Paint.Style.STROKE);
        mPaintBackground.setStrokeWidth(300);
        mPaintBackground.setStrokeCap(Paint.Cap.ROUND);
        mPaintBackground.setColor(Color.BLUE);
    }

    @Override
    public void draw(Canvas canvas) {
        // draw background line
        canvas.drawArc(mRectF, 0, 360, false, mPaintBackground);
        // draw progress line
        canvas.drawArc(mRectF, mStartAngle, mSweepAngle, false, mPaintProgress);
    }

    public void update() {
        mSweepAngle += mStep;
        invalidateSelf();
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(ColorFilter cf) {

    }

    @Override
    public int getOpacity() {
        return 0;
    }
}
