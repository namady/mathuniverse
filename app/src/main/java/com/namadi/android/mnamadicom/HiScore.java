package com.namadi.android.mnamadicom;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class HiScore extends ActionBarActivity {
    private ProgressBar progBar;
    private TextView text;
    private Handler mHandler = new Handler();
    private int mProgressStatus=0;
    int millis;
    int secs = 10;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hi_score);
        progBar= (ProgressBar)findViewById(R.id.progressBar);
        text = (TextView)findViewById(R.id.progStatus);
        button = (Button)findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go(secs);
                secs--;
            }
        });
    }

    int y;
    int z = 0;

    public void dosomething(final int x) {
        mProgressStatus = 0;
        y = 0;
        millis = (x * 1000) / 100;
        y = x - 1;
        new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus < 100) {
                    mProgressStatus += 1;
                    // Update the progress bar
                    mHandler.post(new Runnable() {
                        public void run() {
                            progBar.setProgress(mProgressStatus);
                            text.setText(y + "\'");
                        }
                    });
                    try {
                        Thread.sleep(millis);
                        z += millis;
                        if(z % 1000 == 0)
                            y--;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private void go(int x) {
        mProgressStatus = 0;
        y = 0;
        millis = (x * 1000) / 100;
        y = x - 1;
        final TextView textView = (TextView)findViewById(R.id.progStatus);
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                while (mProgressStatus < 100) {
                    mProgressStatus += 1;
                    mHandler.post(new Runnable() {
                        public void run() {
                            progBar.setProgress(mProgressStatus);
                            textView.setText(y + "\'");
                        }
                    });
                    z += millis;
                    if(z % 1000 == 0)
                        y--;
                    handler.postDelayed(this, millis);
                }
            }
        });
    }
}
