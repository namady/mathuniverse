package com.namadi.android.mnamadicom;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class Play extends Activity implements OnClickListener{

    ProgressBar progBar;
    TextView firstNum, secondNum, operator, level, score, timer;
    Button firstAns, secondAns, thirdAns;
    Random rand = new Random();
    int nLevel = 1;
    int nScore = 0;
    int wrong1, wrong2, correct;
    int a, b, c = 0;
    int secondsToRun = 20;
    private Handler mHandler;
    private int mProgressStatus;
    boolean keepRunning = true;
    int millis;
    int y;
    int z;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        progBar = (ProgressBar)findViewById(R.id.progressBar);
        timer = (TextView)findViewById(R.id.timer);

        firstNum = (TextView)findViewById(R.id.first_num);
        secondNum = (TextView)findViewById(R.id.second_num);
        operator = (TextView)findViewById(R.id.operator);

        level = (TextView)findViewById(R.id.level);
        score = (TextView)findViewById(R.id.score);

        firstAns = (Button)findViewById(R.id.first_ans);
        secondAns = (Button)findViewById(R.id.second_ans);
        thirdAns = (Button)findViewById(R.id.third_ans);

        firstAns.setOnClickListener(this);
        secondAns.setOnClickListener(this);
        thirdAns.setOnClickListener(this);

        generateQuestion();

    }

    @Override
    public void onClick(View v) {
        Thread.currentThread().interrupt();
        progBar.clearAnimation();
        progBar.clearFocus();
        keepRunning = false;
        if(v.getId() == firstAns.getId()) {
            update(firstAns);
        }

        if(v.getId() == secondAns.getId()) {
            update(secondAns);
        }

        if(v.getId() == thirdAns.getId()) {
            update(thirdAns);
        }
        generateQuestion();
    }

    public void generateQuestion() {
        a = rand.nextInt(nLevel * 2);
        b = rand.nextInt(nLevel * 2);

        firstNum.setText(""+a);
        secondNum.setText(""+b);

        int x = (nLevel < 5)?2:3;

        switch (rand.nextInt(x)) {
            case 0:
                operator.setText("-");
                correct = a - b;
                break;
            case 1:
                operator.setText("+");
                correct = a + b;
                break;
            case 2:
                operator.setText("*");
                correct = a * b;
                break;
        }

        switch(rand.nextInt(5)) {
            case 0:
                wrong1 = correct + rand.nextInt(4) + 1;
                wrong2 = correct - rand.nextInt(4) - 1;
                break;
            case 1:
                wrong1 = correct + rand.nextInt(4) + 1;
                wrong2 = correct * rand.nextInt(4) - 1;
                break;
            case 2:
                wrong1 = correct + rand.nextInt(4) + 1;
                wrong2 = correct + rand.nextInt(4) - 1;
                break;
            case 3:
                wrong1 = correct - rand.nextInt(4) + 1;
                wrong2 = correct - rand.nextInt(4) - 1;
                break;
            case 4:
                wrong1 = correct * rand.nextInt(4) + 1;
                wrong2 = correct - rand.nextInt(4) - 1;
                break;
        }

        if(wrong1 == wrong2 || wrong1 == correct|| wrong2 == correct) {
            generateQuestion();
        }


        switch (rand.nextInt(3)) {
            case 0:
                firstAns.setText(""+correct);
                secondAns.setText(""+wrong1);
                thirdAns.setText(""+wrong2);
                break;
            case 1:
                firstAns.setText(""+wrong2);
                secondAns.setText(""+correct);
                thirdAns.setText(""+wrong1);
                break;
            case 2:
                firstAns.setText(""+wrong1);
                secondAns.setText(""+wrong2);
                thirdAns.setText(""+correct);
                break;
        }

        keepRunning = true;
        Thread.currentThread().interrupt();
        dosomething(secondsToRun);
    }

    public void update(Button button) {
        int ans = Integer.parseInt(button.getText().toString());
        if(ans == correct) {
            c++;
            Toast.makeText(getApplicationContext(), "Well done", Toast.LENGTH_SHORT).show();
            nScore += nLevel * (Math.abs(secondsToRun-9));

            if(c > 6) {
                nLevel++;
                c = 0;
            }
        }

        if(ans != correct){
            Toast.makeText(getApplicationContext(), "Sorry", Toast.LENGTH_SHORT).show();
            endGame();
        }

        score.setText("" + nScore);
        level.setText("" + nLevel);
    }

    public void endGame() {
        progBar.setVisibility(View.INVISIBLE);
        setContentView(R.layout.game_over);
        startActivity(new Intent(Play.this, MainActivity.class));
    }

    public void dosomething(final int x) {

        mHandler = new Handler();
        millis = (x * 1000) / 100;
        y = x - 1;
        z = 0;
        mProgressStatus = 0;

        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                while (mProgressStatus < 100 && keepRunning) {

                    if(mProgressStatus == 99)
                        endGame();

                    mProgressStatus += 1;
                    // Update the progress bar
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            progBar.setProgress(mProgressStatus);
                            timer.setText(y + "\'");
                        }
                    });
                    try {
                        Thread.sleep(millis);
                        z += millis;
                        if(z % 1000 == 0)
                            y--;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        new Thread(runnable1).start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        endGame();
    }
}